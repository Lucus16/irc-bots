#!/usr/bin/env python

import irc.connection
import traceback
import importlib
import argparse
import irc.bot
import time
import ssl
import sys

trace = traceback.format_exc

class Message(object):
    def __init__(self, host, src, text, chan=None):
        self.host = host
        self.src = src
        self.original = text
        self.text = text.strip()
        self.chan = chan
        self.hl = False
        if self.chan is None:
            self.hl = True
        if self.text.lower().startswith(self.host.nick.lower() + ':'):
            self.hl = True
            self.text = self.text[len(self.host.nick) + 1:].strip()

    def respond(self, response, hl=None):
        if hl is None: hl = self.hl
        if self.chan is None:
            self.host.send(self.src, response)
        elif hl:
            self.host.send(self.chan, self.src + ': ' + response)
        else:
            self.host.send(self.chan, response)

class BotHost(irc.bot.SingleServerIRCBot):
    def __init__(self, host, port, nick, module):
        self.nick = nick
        if port in [6697]:
            # enable ssl
            ssl_factory = irc.connection.Factory(wrapper=ssl.wrap_socket)
            irc.bot.SingleServerIRCBot.__init__(self, [(host, port)], nick,
                    nick, connect_factory=ssl_factory)
        else:
            irc.bot.SingleServerIRCBot.__init__(self, [(host, port)], nick,
                    nick)
        try:
            self.module = importlib.import_module(module)
            self.handler = self.module.Handler(self)
        except Exception:
            print('Failed to load handler.')
            print(trace())
            exit(1)
        self.logfile = open(module + '.log', 'a')
        def logger(c, e):
            if e.type == 'all_raw_messages':
                return
            line = ' '.join([
                str(e.type),
                str(e.source),
                str(e.target),
                str(e.arguments)])
            self.log(line)
        self.connection.add_global_handler('all_events', logger, 10)

    def die(self, msg=None):
        if msg is not None:
            irc.bot.SingleServerIRCBot.die(self, msg)
        else:
            irc.bot.SingleServerIRCBot.die(self)
        self.logfile.close()

    def on_nicknameinuse(self, c, e):
        c.nick(c.get_nickname() + '_')

    def on_welcome(self, c, e):
        c.join('#' + self.nick)

    def on_privmsg(self, c, e):
        m = Message(self, e.source.nick, e.arguments[0])
        self.handle(m)

    def on_pubmsg(self, c, e):
        m = Message(self, e.source.nick, e.arguments[0], e.target)
        self.handle(m)

    def on_action(self, c, e):
        pass #TODO: This handles /me

    def send(self, target, message):
        self.connection.notice(target, message)

    def on_join(self, c, e):
        pass #TODO

    def handle(self, msg):
        if self.handle_builtins(msg): return
        try:
            return self.handler.handle(msg)
        except Exception:
            self.log('!!! Handler failed.\n' + trace())
            return

    def log(self, m):
        line = time.ctime() + ' ' + m + '\n'
        self.logfile.write(line)
        print(line, end='')

    def handle_builtins(self, msg):
        if msg.hl:
            text = msg.text
            if text.lower().startswith('die'):
                diemsg = msg[4:].strip()
                if diemsg == '': diemsg = 'Killed by ' + msg.src + '.'
                self.die(diemsg)
                msg.respond("I can't die, muhaha!")
                return True
            elif text.lower().startswith('reload'):
                if self.reload():
                    msg.respond('Reloading successful.')
                else:
                    msg.respond('Reloading failed.')
                return True
        return False

    def reload(self):
        try:
            self.module = importlib.reload(self.module)
            self.handler = self.module.Handler(self, prev=self.handler)
        except Exception:
            self.log('Reload failed.\n' + trace())
            return False
        self.log('Reload successful.')
        return True

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Run an irc bot.')
    parser.add_argument('hostname', help='The irc server hostname.')
    parser.add_argument('port', type=int, help='The irc server port.')
    parser.add_argument('nickname', help='The nickname used by the bot.')
    parser.add_argument('module',
            help='The module that describes the behavior of the bot.')
    args = parser.parse_args()
    bot = BotHost(args.hostname, args.port, args.nickname, args.module)
    try:
        bot.start()
    except KeyboardInterrupt:
        bot.die('Shutting down.')
    except Exception:
        bot.log(trace())
        bot.die('Crashed and burned.')
